#!/bin/sh

ANSIBLE_ROLES_PATH=./roles \
  ansible -b -vv localhost \
  -e infra_server_db_host=192.168.178.20 \
  -e infra_server_ui_proxy=http://192.168.178.20:3000 \
  --module-name include_role --args name=infra
